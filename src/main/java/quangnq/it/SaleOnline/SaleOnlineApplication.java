package quangnq.it.SaleOnline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaleOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaleOnlineApplication.class, args);
	}

}
